import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        return (
            <div style={{position: 'relative', bottom: '0px'}} >
                <p style={{alignContent: 'center'}}>&copy; Copyright 2020-21 by Jithin Modon</p>
            </div>
        )
    }
}

export default Footer

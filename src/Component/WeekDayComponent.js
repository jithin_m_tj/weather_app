import React, { Component } from 'react';


import DailyHourData from './DailyHourData';
import DayComponent from './DayComponent';
import * as ApiConstants from './ApiConstants';

class WeekDayComponent extends Component {

  constructor(props) {
    super(props)
    this.handler = this.handler.bind(this)
  }

    state = {
        completeData: [],
        dailyData: [],
        dateValue: "2020-09-06",
        hourValue: "12:00:00",
        loading: true
      }
    
  handler(date) {
    this.setState({dateValue: date})
  }
      componentDidMount = () => {
        const weatherURL = ApiConstants['API_MAIN_URL']+'zip='+ApiConstants['ZIP_CODE']+','+ApiConstants['COUNTRY_CODE']+'&units='+ApiConstants['METRIC']+'&APPID='+ApiConstants['APP_KEY'];
        fetch(weatherURL)
        .then(res => res.json())
        .then(data => {
          const dailyData = data.list.filter(reading => reading.dt_txt.includes(this.state.hourValue))
          this.setState({
            completeData: data.list,
            dailyData: dailyData,
            loading: false
          }, () => console.log(this.state)) 
        })
      }

      individualCard = () => {
        return this.state.dailyData.map((reading) => 
        <DayComponent handler = {this.handler}  reading={reading} key={reading.dt_txt.split(' ')[0]} />)
      }

    render() {
        
      return (
            <div  style={{display: "inline"}}>
              {this.state.loading === true ? <div className="loader" style={{textAlign: 'center'}}></div>
              :
              <div style={{display: "table-row"}}>
              {this.individualCard()}
                </div>
              }
              <DailyHourData dateValue={this.state.dateValue} completeData={this.state.completeData} />
            </div>
        )
    }
}

export default WeekDayComponent;

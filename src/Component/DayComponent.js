import React, { Component } from 'react';
import {
    Link
  } from "react-router-dom";

class DayComponent extends Component {
    
    render() {
        const dayNames = [
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat'
          ];
          
        const selectedDate = this.props.reading.dt_txt.split(' ')[0];//first index will be date of dt_txt
        const selectedTime = this.props.reading.dt_txt.split(' ')[1];//second index will be time of dt_txt
        const hour = selectedTime.split(':')[0];
        var suffix = hour >= 12 ? " PM":" AM";
        var hours = ((parseInt(hour) + 11) % 12 + 1) + suffix
        const dateObject = new Date(selectedDate);
        const dayName = dayNames[dateObject.getDay()];
        
        const minTemp = this.props.reading.main.temp_min
        const maxTemp = this.props.reading.main.temp_max
        const currentTemp = this.props.reading.main.temp

        const imageName = this.props.reading.weather[0].main;
        const description = this.props.reading.weather[0].description;

        return (
            <Link style={{ textDecoration: 'none' }} to={"/"+dayName} key={selectedDate} data-date={selectedDate} onClick={() =>  this.props.handler(selectedDate)}>
            <div className="card" key={selectedDate}  style={{display: "table-cell", background: "skyblue" }}>
                <div className="container">
                    <b style={{color: 'red'}}>{ dayName }</b>
                </div>
                <div className="container">
                    {selectedDate}
                </div>
                <div className="container"  style={{color: 'green'}}>
                    {hours}
                </div>
                <img alt='imag' src={require(`../assets/img/${imageName}.png`)} />                        
                <div className="container">
                    <b style={{color: "black"}}>{ description }</b><br/><br/>
                </div>
                <div className="container">
                    <label style={{padding: "15px", margin: "5px", color: "green"}}>{currentTemp} &deg;C </label>    
                </div>
                <div className="container">
                    <label style={{padding: "15px", margin: "5px", color: "black"}}>{maxTemp} &deg;C </label>    
                    <label style={{padding: "15px", margin: "5px"}}>{minTemp} &deg;C </label>
                </div>
            </div>
            </Link>
        )
    }
}

export default DayComponent;

import React, { Component } from 'react';

import DayComponent from './DayComponent';

class DailyHourData extends Component {

    individualHour = () => {
        const displayData = this.props.completeData.filter(reading => reading.dt_txt.includes(this.props.dateValue))
        return displayData.map((reading) => 
        <DayComponent reading={reading} key={reading.dt_txt} />)
    }

    render() {
        return (
            <div className="dailyDataCard  HourlyBoard">
                <h2 style={{color: "white"}}> Hourly Forecast</h2>
                {this.individualHour()}
            </div>
        )
    }
}

export default DailyHourData;

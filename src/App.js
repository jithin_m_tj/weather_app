import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './App.css';
import WeekDayComponent from './Component/WeekDayComponent';
import Header from './Component/Header';
import Footer from './Component/Footer';


function App() {
  return (
    <Router>
      <div className="App"  style={{textAlign: 'center'}}>
        <Header/>
          <Switch>
            <Route path="/">
            <div  className="WeekBoard" style={{display: 'table',textAlign: 'center',width: "100%"}}>
          <WeekDayComponent />
        </div>
            </Route>
          </Switch>
        
        <Footer />
      </div>
    </Router>
  );
}



export default App;
